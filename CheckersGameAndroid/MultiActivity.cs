using System;
using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Util;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Java.Lang;

using Checkers.Multipeer.Connection;
using Checkers.Viewer.Providers.Controls;

namespace CheckersGameAndroid
{
	/// <summary>
	/// This is the main Activity that displays the current chat session.
	/// </summary>
	[Activity(Label = "CheckersGame", Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]    
	public class MultiActivity : Activity
	{
		// Debugging
		private const string TAG = "MultiActivity";
		private const bool Debug = true;
		
		// Message types sent from the BluetoothChatService Handler
		// TODO: Make into Enums
		public const int MESSAGE_STATE_CHANGE = 1;
		public const int MESSAGE_READ = 2;
		public const int MESSAGE_WRITE = 3;
		public const int MESSAGE_DEVICE_NAME = 4;
		public const int MESSAGE_TOAST = 5;
		
		// Key names received from the BluetoothChatService Handler
		public const string DEVICE_NAME = "device_name";
		public const string TOAST = "toast";
		
		// Intent request codes
		// TODO: Make into Enums
		private const int REQUEST_CONNECT_DEVICE = 1;
		private const int REQUEST_ENABLE_BT = 2;

		// Name of the connected device
		protected string connectedDeviceName = null;
		// Array adapter for the conversation thread
		protected ArrayAdapter<string> conversationArrayAdapter;
		// String buffer for outgoing messages
		private StringBuffer outStringBuffer;
		// Local Bluetooth adapter
		private BluetoothAdapter bluetoothAdapter = null;
		// Member object for the chat services
		private BluetoothService chatService = null;

		private BoardPanel2D view;
		
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
            RequestWindowFeature(WindowFeatures.NoTitle);
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);
			if (Debug)
				Log.Error (TAG, "+++ ON CREATE +++");
			
			// Set up the window layout
			SetContentView (Resource.Layout.Single);
						
			// Get local Bluetooth adapter
			bluetoothAdapter = BluetoothAdapter.DefaultAdapter;
			
			// If the adapter is null, then Bluetooth is not supported
			if (bluetoothAdapter == null) {
				Toast toast = Toast.MakeText (Application.Context, "Bluetooth is not available", ToastLength.Long);
				toast.SetGravity (GravityFlags.Center, 0, 0);
				toast.Show ();
				Finish ();
				return;
			}

			ImageButton btnMenu = FindViewById<ImageButton> (Resource.Id.btnMenu);
			btnMenu.Click += delegate
			{
				Finish();
			};
			
			var metrics = Resources.DisplayMetrics;
			int w = metrics.WidthPixels;
			int h = metrics.HeightPixels;
			int size = w > h ? h : w;
			size = (int)(size * 1.0);
			
			view = new BoardPanel2D (this);
			LinearLayout.LayoutParams para = new LinearLayout.LayoutParams (size, size);
			view.LayoutParameters = para;
			view.init (MainActivity.globalOption.FirstPlayer, MainActivity.globalOption.Level, 2, size);

			view.SendRequest += (sender, e) => {
				var msg = new Java.Lang.String(e.Value);
				SendMessage(msg);
			};
			
			LinearLayout cotainer = FindViewById<LinearLayout> (Resource.Id.container);
			cotainer.AddView (view);

			Toast toast1 = Toast.MakeText (Application.Context, "The Status is Disconnect, Please connect a device in OptionMenu.", ToastLength.Long);
			toast1.SetGravity (GravityFlags.Center, 0, 0);
			toast1.Show ();
		}
		
		protected override void OnStart ()
		{
			base.OnStart ();
			
			if (Debug)
				Log.Error (TAG, "++ ON START ++");
			
			// If BT is not on, request that it be enabled.
			// setupChat() will then be called during onActivityResult
			if (!bluetoothAdapter.IsEnabled) {
				Intent enableIntent = new Intent (BluetoothAdapter.ActionRequestEnable);
				StartActivityForResult (enableIntent, REQUEST_ENABLE_BT);
				// Otherwise, setup the chat session
			} else {
				if (chatService == null)
					SetupChat ();
			}
		}
		
		protected override void OnResume ()
		{
			base.OnResume ();
			
			// Performing this check in onResume() covers the case in which BT was
			// not enabled during onStart(), so we were paused to enable it...
			// onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
			if (chatService != null) {
				// Only if the state is STATE_NONE, do we know that we haven't started already
				if (chatService.GetState () == 0) {
					// Start the Bluetooth chat services
					chatService.Start ();
				}
			}
		}
		
		private void SetupChat ()
		{
			Log.Debug (TAG, "SetupChat()");

			// Initialize the BluetoothChatService to perform bluetooth connections
			chatService = new BluetoothService (this, new MyHandler (this));
			
			// Initialize the buffer for outgoing messages
			outStringBuffer = new StringBuffer ("");
		}
		
		protected override void OnPause ()
		{
			base.OnPause ();
			
			if (Debug)
				Log.Error (TAG, "- ON PAUSE -");
		}
		
		protected override void OnStop ()
		{
			base.OnStop ();
			
			if(Debug)
				Log.Error (TAG, "-- ON STOP --");
		}
		
		protected override void OnDestroy ()
		{
			base.OnDestroy ();
			
			// Stop the Bluetooth chat services
			if (chatService != null)
				chatService.Stop ();
			
			if (Debug)
				Log.Error (TAG, "--- ON DESTROY ---");
		}
		
		private void EnsureDiscoverable ()
		{
			if (Debug)
				Log.Debug (TAG, "ensure discoverable");
			
			if (bluetoothAdapter.ScanMode != ScanMode.ConnectableDiscoverable) {
				Intent discoverableIntent = new Intent (BluetoothAdapter.ActionRequestDiscoverable);
				discoverableIntent.PutExtra (BluetoothAdapter.ExtraDiscoverableDuration, 300);
				StartActivity (discoverableIntent);
			}
		}
		
		/// <summary>
		/// Sends a message.
		/// </summary>
		/// <param name='message'>
		/// A string of text to send.
		/// </param>
		private void SendMessage (Java.Lang.String message)
		{
			// Check that we're actually connected before trying anything
			if (chatService == null || chatService.GetState () != 3) {
				Toast toast = Toast.MakeText (Application.Context, Resource.String.not_connected, ToastLength.Long);
				toast.SetGravity (GravityFlags.Center, 0, 0);
				toast.Show ();
				return;
			}
			
			// Check that there's actually something to send
			if (message.Length () > 0) {
				// Get the message bytes and tell the BluetoothChatService to write
				byte[] send = message.GetBytes ();
				chatService.Write (send);
				
				// Reset out string buffer to zero and clear the edit text field
				outStringBuffer.SetLength (0);
//				outEditText.Text = string.Empty;
			}
		}
		
		
		// The Handler that gets information back from the BluetoothChatService
		private class MyHandler : Handler
		{
			MultiActivity bluetoothChat;
			
			public MyHandler (MultiActivity chat)
			{
				bluetoothChat = chat;	
			}
			
			public override void HandleMessage (Message msg)
			{
				switch (msg.What) {
				case MESSAGE_STATE_CHANGE:
					if (Debug)
						Log.Info (TAG, "MESSAGE_STATE_CHANGE: " + msg.Arg1);
					switch (msg.Arg1) {
							case 3:
//						bluetoothChat.title.SetText (Resource.String.title_connected_to);
//						bluetoothChat.title.Append (bluetoothChat.connectedDeviceName);
						break;
							case 2:
//						bluetoothChat.title.SetText (Resource.String.title_connecting);
						//bluetoothChat.view.GameStarted = false;
						break;
							case 1:
							case 0:
//						bluetoothChat.title.SetText (Resource.String.title_not_connected);
						break;
					}
					break;
				case MESSAGE_WRITE:
					byte[] writeBuf = (byte[])msg.Obj;
					// construct a string from the buffer
					var writeMessage = new Java.Lang.String (writeBuf);
					break;
				case MESSAGE_READ:
					byte[] readBuf = (byte[])msg.Obj;
					// construct a string from the valid bytes in the buffer
					var readMessage = new Java.Lang.String (readBuf, 0, msg.Arg1);
					string str = (string)readMessage;
					bluetoothChat.view.ReceiveData(str);
					break;
				case MESSAGE_DEVICE_NAME:
					// save the connected device's name
					bluetoothChat.connectedDeviceName = msg.Data.GetString (DEVICE_NAME);
					Toast toast1 = Toast.MakeText (Application.Context, "Connected to " + bluetoothChat.connectedDeviceName, ToastLength.Long);
					toast1.SetGravity (GravityFlags.Center, 0, 0);
					toast1.Show ();
					break;
				case MESSAGE_TOAST:
					bluetoothChat.view.GameStarted = false;
					bluetoothChat.view.RefreshBoard();
					Toast toast2 = Toast.MakeText (Application.Context, msg.Data.GetString (TOAST), ToastLength.Long);
					toast2.SetGravity (GravityFlags.Center, 0, 0);
					toast2.Show ();
					break;
				}
			}
		}
		
		protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			if (Debug)
				Log.Debug (TAG, "onActivityResult " + resultCode);
			
			switch(requestCode)
			{
			case REQUEST_CONNECT_DEVICE:
				// When DeviceListActivity returns with a device to connect
				if( resultCode == Result.Ok)
				{
					// Get the device MAC address
					var address = data.Extras.GetString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
					// Get the BLuetoothDevice object
					BluetoothDevice device = bluetoothAdapter.GetRemoteDevice(address);
					// Attempt to connect to the device
					chatService.Connect(device);
				}
				break;
			case REQUEST_ENABLE_BT:
				// When the request to enable Bluetooth returns
				if(resultCode == Result.Ok)
				{
					// Bluetooth is now enabled, so set up a chat session
					SetupChat();	
				}
				else
				{
					// User did not enable Bluetooth or an error occured
					Log.Debug(TAG, "BT not enabled");
					Toast toast = Toast.MakeText (Application.Context, Resource.String.bt_not_enabled_leaving, ToastLength.Long);
					toast.SetGravity (GravityFlags.Center, 0, 0);
					toast.Show ();
					Finish();
				}
				break;
			}
		}
		
		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			var inflater = MenuInflater;
			inflater.Inflate(Resource.Menu.option_menu, menu);
			return true;
		}
		
		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) 
			{
			case Resource.Id.scan:
				// Launch the DeviceListActivity to see devices and do scan
				var serverIntent = new Intent(this, typeof(DeviceListActivity));
				StartActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
				return true;
//			case Resource.Id.discoverable:
//				// Ensure this device is discoverable by others
//				EnsureDiscoverable();
//				return true;
			}
			return false;
		}
	}
}


