
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Checkers.Viewer.Providers.Controls;

namespace CheckersGameAndroid
{
	[Activity(Label = "CheckersGame", Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]	
	public class SingleActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
            RequestWindowFeature(WindowFeatures.NoTitle);
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);

			// Create your application here
			SetContentView(Resource.Layout.Single);

			ImageButton btnMenu = FindViewById<ImageButton> (Resource.Id.btnMenu);
			btnMenu.Click += delegate
			{
				Finish();
			};

			var metrics = Resources.DisplayMetrics;
			int w = metrics.WidthPixels;
			int h = metrics.HeightPixels;
			int size = w > h ? h : w;
			size = (int)(size * 1.0);

			BoardPanel2D view = new BoardPanel2D (this);
			LinearLayout.LayoutParams para = new LinearLayout.LayoutParams (size, size);
			view.LayoutParameters = para;
			view.init (MainActivity.globalOption.FirstPlayer, MainActivity.globalOption.Level, 1, size);

			LinearLayout cotainer = FindViewById<LinearLayout> (Resource.Id.container);
			cotainer.AddView (view);

			Toast toast = Toast.MakeText (Application.Context, "Click Your Piece To Play", ToastLength.Long);
			toast.SetGravity (GravityFlags.Center, 0, 0);
			toast.Show ();
		}
	}
}

