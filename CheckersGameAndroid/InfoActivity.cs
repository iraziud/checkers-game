
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CheckersGameAndroid
{
	[Activity(Label = "CheckersGame", Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]    
	public class InfoActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
            RequestWindowFeature(WindowFeatures.NoTitle);
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);
			// Create your application here
			SetContentView (Resource.Layout.Info);
			
			//textview done
			TextView tv = FindViewById<TextView> (Resource.Id.tvDone);
			tv.Click += delegate {
				Finish();
			};
		}
	}
}

