
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CheckersGameAndroid
{
	[Activity(Label = "CheckersGame", Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]    		
	public class OptionsActivity : Activity
	{
		private string [] levels = new string[3]{"Easy", "Normal", "Hard"};
		TextView tvLevel;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
            RequestWindowFeature(WindowFeatures.NoTitle);
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);
			// Create your application here
			SetContentView (Resource.Layout.Options);

			//textview done
			TextView tv = FindViewById<TextView> (Resource.Id.tvDone);
			tv.Click += delegate {
				Finish();
			};

			//first togglebutton
			Switch swFirst = FindViewById<Switch> (Resource.Id.swFirst);

			if (MainActivity.globalOption.FirstPlayer == 1)
				swFirst.Checked = true;
			else
				swFirst.Checked = false;

			swFirst.Click += delegate {
				if(swFirst.Checked)
					MainActivity.globalOption.FirstPlayer = 1;
				else
					MainActivity.globalOption.FirstPlayer = 2;
				Console.WriteLine (MainActivity.globalOption.FirstPlayer);
			};

			//seekbar level
			SeekBar sbLevel = FindViewById<SeekBar> (Resource.Id.sbLevel);

			sbLevel.Progress = MainActivity.globalOption.Level - 2;

			sbLevel.ProgressChanged += delegate(object sender, SeekBar.ProgressChangedEventArgs e) {
				MainActivity.globalOption.Level = e.Progress + 2;
				tvLevel.Text = levels [sbLevel.Progress];
				Console.WriteLine(MainActivity.globalOption.Level);
			};		

			//textview level
			tvLevel = FindViewById<TextView> (Resource.Id.tvLevel);

			tvLevel.Text = levels [sbLevel.Progress];
		}
	}
}

