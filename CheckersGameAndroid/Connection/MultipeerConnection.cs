using System;
using System.Drawing;

namespace Checkers.Multipeer.Connection
{
    //Avoid needless subclassing
    public class TArgs<T> : EventArgs
    {
        public T Value { get; protected set; }

        public TArgs(T value)
        {
            this.Value = value;
        }
    }
    //One browser, many advertisers
    public enum Role
    {
        Advertiser,
        Browser
    }
    //Make messages easy to subscribe to
    public interface IMessager
    {
        event EventHandler<TArgs<string>> MessageReceived;
    }
/*

    public class ReceivedEventArgs : EventArgs
    {
        public MCSession Session { get; protected set; }

        public NSData Data { get; protected set; }

        public MCPeerID Sender { get; protected set; }

        public ReceivedEventArgs(MCSession session, NSData data, MCPeerID sender)
        {
            this.Session = session;
            this.Data = data;
            this.Sender = sender;
        }
    }

    public class AdvertiserController : DiscoveryViewController
    {
        MCNearbyServiceAdvertiser advertiser;

        public AdvertiserController(string peerID) : base(peerID)
        {
            PeerID = new MCPeerID("Server:" + peerID);
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var dis = new DiscoveryView("Advertiser", this);
            View = dis;
            var emptyDict = new NSDictionary();
            Status("Starting advertising...");
            dis.Done += (s, e) =>
            {
                DismissViewController(true, null);
            };

            advertiser = new MCNearbyServiceAdvertiser(PeerID, emptyDict, SERVICE_STRING);
            advertiser.Delegate = new MyNearbyAdvertiserDelegate(this);
            advertiser.StartAdvertisingPeer();
        }
    }

    class MyNearbyAdvertiserDelegate : MCNearbyServiceAdvertiserDelegate
    {
        AdvertiserController parent;

        public MyNearbyAdvertiserDelegate(AdvertiserController parent)
        {
            this.parent = parent;
        }

        public override void DidReceiveInvitationFromPeer(MCNearbyServiceAdvertiser advertiser, MCPeerID peerID, NSData context, MCNearbyServiceAdvertiserInvitationHandler invitationHandler)
        {
            parent.Status("Received Invite...");
            InvokeOnMainThread(() => invitationHandler(true, parent.Session));
        }
    }

    public class BrowserController : DiscoveryViewController
    {
        MCNearbyServiceBrowser browser;

        public BrowserController(string peerID) : base(peerID)
        {
            PeerID = new MCPeerID("Client:" + peerID);
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var brows = new DiscoveryView("Browser", this);
            View = brows;
            brows.Done += (s, e) => 
            {
                DismissViewController(true, null);
            };
            browser = new MCNearbyServiceBrowser(PeerID, SERVICE_STRING);
            browser.Delegate = new MyBrowserDelegate(this);

            Status("Starting browsing...");
            browser.StartBrowsingForPeers();
        }

        class MyBrowserDelegate : MCNearbyServiceBrowserDelegate
        {
            BrowserController parent;
            NSData context;

            public MyBrowserDelegate(BrowserController parent)
            {
                this.parent = parent;
                context = new NSData();
            }

            public override void FoundPeer(MCNearbyServiceBrowser browser, MCPeerID peerID, NSDictionary info)
            {
                parent.Status("Connecting ... " + peerID.DisplayName);
                InvokeOnMainThread(() => browser.InvitePeer(peerID, parent.Session, context, 20));

            }

            public override void LostPeer(MCNearbyServiceBrowser browser, MCPeerID peerID)
            {
                parent.Status("Lost peer " + peerID.DisplayName);
            }

            public override void DidNotStartBrowsingForPeers(MCNearbyServiceBrowser browser, NSError error)
            {
                parent.Status("DidNotStartBrowingForPeers " + error.Description);
            }
        }
    }
	*/
}

