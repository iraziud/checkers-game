using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace CheckersGameAndroid
{
	[Activity(Label = "CheckersGame", Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]    
    public class MainActivity : Activity
    {
		public static OptionInfo globalOption = new OptionInfo (3, 1, true);

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            RequestWindowFeature(WindowFeatures.NoTitle);
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it            
            ImageButton btnSingle = FindViewById<ImageButton>(Resource.Id.btnSingle);
            btnSingle.Click += delegate
            {
                Console.WriteLine("Single Button pressed!!!");
				Intent activity = new Intent(this, typeof(SingleActivity));
				StartActivity(activity);
            };
            ImageButton btnMutilple = FindViewById<ImageButton>(Resource.Id.btnMultiple);
            btnMutilple.Click += delegate
            {
                Console.WriteLine("Multiple Button pressed!!!");
				Intent activity = new Intent(this, typeof(MultiActivity));
				StartActivity(activity);
            };
            ImageButton btnOptions = FindViewById<ImageButton>(Resource.Id.btnOptions);
            btnOptions.Click += delegate
            {
                Console.WriteLine("Options Button pressed!!!");
				Intent activity = new Intent(this, typeof(OptionsActivity));
				StartActivity(activity);
            };
			ImageButton btnInfo = FindViewById<ImageButton>(Resource.Id.btnInfo);
			btnInfo.Click += delegate
			{
				Console.WriteLine("Info Button pressed!!!");
				Intent activity = new Intent(this, typeof(InfoActivity));
				StartActivity(activity);
			};
        }
    }
}


