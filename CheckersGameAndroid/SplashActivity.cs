
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CheckersGameAndroid
{
	[Activity(MainLauncher = true, Theme = "@style/Theme.Splash", NoHistory = true, Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]	
	public class SplashActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);
			// Create your application here
			MyHandler handler = new MyHandler (this);
			handler.SendEmptyMessageDelayed (MESSAGE_MAIN, 3000);
		}

		public const int MESSAGE_MAIN = 1;

		private class MyHandler : Handler
		{
			SplashActivity splash;
			
			public MyHandler (SplashActivity _splash)
			{
				splash = _splash;	
			}
			
			public override void HandleMessage (Message msg)
			{
				switch (msg.What) {
				case MESSAGE_MAIN:
					splash.StartActivity(typeof(MainActivity));
					splash.Finish();
					break;				
				}
			}
		}
	}
}

