using System;
using System.Drawing;
using System.Diagnostics;
using System.IO;

using Android.Media;
using Android.Content;

namespace Checkers.Viewer.Providers.Controls
{
   /// <summary>
   /// Win 32 Audio player implementation
   /// </summary>
   public class AudioPlayer
   {      
      MediaPlayer player;      

      public void PlayWavResource(Context ctx, int resId)
      {        
		player = MediaPlayer.Create(ctx, resId);
		player.Start();                        
      }
  }
}
