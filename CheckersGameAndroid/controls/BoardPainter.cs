using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Drawing;

using Checkers.Model;
using Checkers.Viewer.Providers.ViewModel;

using Android.App;
using Android.Graphics;
using Android.Content.Res;

using CheckersGameAndroid;

namespace Checkers.Viewer.Providers.Controls
{
   internal class BoardPainter : IDisposable
   {
      private Bitmap boardImage;
      private int imageInset = 1;
      private IPieceSet pieceSet;
		private int width;
      
      public BoardPainter(IPieceSet pieceSet, int width)
      {
         if (pieceSet == null)
         {
            throw new ArgumentNullException("pieceSet");
         }

         this.pieceSet = pieceSet;
			this.width = width;
      }

      
      public virtual void Dispose()
      {
         this.ClearCache();
         System.GC.SuppressFinalize(this);
      }
      

      public void ClearCache()
      {
         if (this.boardImage != null)
         {
            this.boardImage.Dispose();
         }
         this.boardImage = null;
      }

	  public void Paint(Canvas context, IBoard board, int squareSize, FloatingPiece floatingPiece)
      {
         Bitmap boardImageCopy = this.boardImage;
         if (boardImageCopy == null)
         {
			Resources res = Application.Context.Resources;
			boardImageCopy = this.boardImage = Util.DecodeSampledBitmapFromResource(res, Resource.Drawable.mainboard, 312, 312);
         }

			System.Drawing.PointF firstPos = new System.Drawing.PointF(0, 0);
			RectangleF imageRect = new RectangleF (firstPos.X, firstPos.Y, boardImageCopy.Width, boardImageCopy.Height);
			//context.DrawBitmap (boardImage, firstPos.X, firstPos.Y, new Paint());
			context.DrawBitmap (boardImage, null, new Rect(0,0,width,width), new Paint());
			Paint_piece(context, board, squareSize, floatingPiece);
      }

      private static int GetBoardWidth(int squareSize)
      {
         return BoardConstants.Cols * squareSize;
      }

      private static int GetBoardHeight(int squareSize)
      {
         return BoardConstants.Rows * squareSize;
      }

      private void Paint_piece(Canvas context, IBoard board, int squareSize, FloatingPiece floatingPiece)
      {
         int x = 0;
         int y = 0;
         int imageSize = squareSize - imageInset * 2;
         int position = BoardConstants.LightSquareCount;
		 Rect rect;
		
         for (int row = 0; row < BoardConstants.Rows; row++)
         {
            x = 0;

            for (int col = 0; col < BoardConstants.Rows; col++)
            {
               bool isDarkSquare = (row % 2 != col % 2);
               //
               // Draw square
               //
               if (isDarkSquare)
               {
                  //
                  // Draw piece on square
                  //
                     Piece piece = board[position];
                     if ((piece != Piece.None) && (floatingPiece.Position != position))
                     {
							rect = new Rect (x + imageInset, y + imageInset, x + imageInset+imageSize, y + imageInset+imageSize);                        
						context.DrawBitmap(GetPieceImage(piece), null, rect, new Paint());
                     }

                  position--;
               }
               x += squareSize;
            }

            y += squareSize;
         }
         //
         // draw floating piece
         //
            if ((floatingPiece.Active) && (BoardUtilities.IsPiece(board[floatingPiece.Position])))
            {
               int floatingPieceSize = (int)(imageSize * 1.2);
               const int shadowOffset = 6;
               Bitmap floatingPieceImage = GetPieceImage(board[floatingPiece.Position]);
            
				RectF ovalRect = new RectF(floatingPiece.X + shadowOffset, floatingPiece.Y + shadowOffset, floatingPiece.X + shadowOffset+floatingPieceSize, floatingPiece.Y + shadowOffset+floatingPieceSize);
				context.DrawOval(ovalRect, new Android.Graphics.Paint());
				rect = new Rect (floatingPiece.X + imageInset, floatingPiece.Y + imageInset, floatingPiece.X + imageInset+floatingPieceSize, floatingPiece.Y + imageInset+floatingPieceSize);
				context.DrawBitmap(floatingPieceImage, null, rect, new Paint());
            }

      }



      /// <summary>
      /// Get the image for the specified piece
      /// </summary>
      /// <param name="piece">the piece to get image for</param>
      /// <returns>The image for the piece</returns>
      private Bitmap GetPieceImage(Piece piece)
      {
         switch (piece)
         {
            case Piece.BlackMan:
               return pieceSet.BlackMan;
            case Piece.WhiteMan:
               return pieceSet.WhiteMan;
            case Piece.BlackKing:
               return pieceSet.BlackKing;
            case Piece.WhiteKing:
               return pieceSet.WhiteKing;
            default:
               return null;
         }
      }

      /// <summary>
      /// Get the image for a square
      /// </summary>
      /// <param name="darkSquare">Is this a dark square</param>
      /// <returns>Dark square image iff <c>true</c> and light square if otherwise</returns>
      private Bitmap GetSquareImage(bool darkSquare)
      {
         return (darkSquare) ? pieceSet.DarkSquare : pieceSet.LightSquare;
      }
   }
}
