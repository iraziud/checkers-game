using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;

using System.Text;
using System.Globalization;
using Checkers.Model;
using Checkers.Viewer.Interfaces;
using Checkers.Presenter;
using Checkers.Viewer.Providers.ViewModel;
using Checkers.Multipeer.Connection;
using Android.Views;
using Android.Graphics;
using Android.Content;
using CheckersGameAndroid;

namespace Checkers.Viewer.Providers.Controls
{
   /// <summary>
   /// 2D board panel
   /// </summary>
	public partial class BoardPanel2D : View, IBoardView
   {
      private delegate void RefreshDelegate();
      private delegate void RenderMoveDelegate(Move move,IBoard after);

      public int Mode { get; set; }
      public int FirstPlayer { get; set; }
      public int Level{ get; set; }
      public bool receiveFlag{ get; set;}

      public event EventHandler<TArgs<string>> SendRequest = delegate {};

      #region Fields
      private  BoardPresenter presenter;
      private  IBoard board;
      private  MoveAnimator moveAnimator;
      private  FloatingPiece floatingPiece = new FloatingPiece();
      private  BoardPainter boardPainter;
      private int? startPosition;
      private bool blackLocked;
      private bool whiteLocked;
	  public bool GameStarted { get; set;}
      private string gameMessage;
      public int BoardLength { get; set; }
      private AudioPlayer audio = new AudioPlayer();
      #endregion Fields

		private Context ctx;

		public BoardPanel2D (Context context)
			: base(context, null, 0)
		{
			ctx = context;
		}

		public void init(int first, int level, int mode, int width )
		{
			FirstPlayer = first;
			Level = level;
			Mode = mode;
			BoardLength = width;

			board = new Checkerboard();
			boardPainter = new BoardPainter(PieceSetFactory.CreateWoodGrainPieceSet(), width);
			gameMessage = "Click Your Piece To Play";
			moveAnimator = new MoveAnimator(this, TimeSpan.FromMilliseconds(20), floatingPiece);
			moveAnimator.MoveCompleted += new MoveAnimator.MoveCompletedDelegate(moveAnimator_MoveCompleted);
			presenter = new BoardPresenter(this);
		}

      /// <summary>
      /// Get size of board square
      /// </summary>
      private int SquareSize
      {
         get
         {
				return BoardLength / BoardConstants.Rows;
         }
      }
	
      private string GameMessage
      {
         set
         {
            gameMessage = value;
			RefreshBoard ();
         }

         get { return gameMessage; }
      }


      #region Paint Board
      /// <summary>
      /// Raises the Draw event
      /// </summary>
		protected override void OnDraw (Canvas canvas)
		{
			base.OnDraw (canvas);
			boardPainter.Paint (canvas, board, SquareSize, floatingPiece);
			Start();
		}
    #endregion
     public void Start()
        {
            if (!GameStarted)
            {
                Player humanPlayer = BoardUtilities.GetPlayer( Piece.BlackMan);
                Player computerPlayer = BoardUtilities.GetOpponent(humanPlayer);
                presenter.SetLevel (humanPlayer, Level);
                presenter.SetLevel (computerPlayer, Level);

                if (Mode == 1)
                { //single mode
                    presenter.SetComputer(humanPlayer, false);
                    presenter.SetComputer(computerPlayer, true);
                }
                else
                {
                    if (Mode == 2)
                    { //multi mode
                        presenter.SetComputer(humanPlayer, false);
                        presenter.SetComputer(computerPlayer, false);

                    }
                    else {
                        return;
                    }
                }

                presenter.FirstPlayer = FirstPlayer;
                presenter.StartGame();
                return;
            }
        }
	#region Touches Event Handlers
		public override bool OnTouchEvent (MotionEvent ev)
		{
			if (Mode == 2 && BluetoothService._state != 3) {
				return true;
			}

			MotionEventActions action = ev.Action & MotionEventActions.Mask;
			System.Drawing.PointF pt = new System.Drawing.PointF(ev.GetX (), ev.GetY ());
			int position = GetPosition(pt);
			switch (action) {
			case MotionEventActions.Down:

				if(position > 0)
				{
					Piece piece = board[position];
					bool locked = ((blackLocked && BoardUtilities.IsBlack(piece)) || (whiteLocked && BoardUtilities.IsWhite(piece)));

					if ((!locked) && (piece != Piece.None))
					{
						receiveFlag = false;
						int offset = SquareSize / 2;
						floatingPiece.X = (int)pt.X - offset;
						floatingPiece.Y = (int)pt.Y - offset;
						floatingPiece.Position = position;
						RefreshBoard ();
					}
				}
				break;
				
			case MotionEventActions.Move:

				if ((!moveAnimator.Running) && (floatingPiece.Active)) {
					Piece piece = board [floatingPiece.Position];
					bool locked = ((blackLocked && BoardUtilities.IsBlack (piece)) || (whiteLocked && BoardUtilities.IsWhite (piece)));
					
					if (!locked) {
						int offset = SquareSize / 2;
						floatingPiece.X = (int)pt.X - offset;
						floatingPiece.Y = (int)pt.Y - offset;
					}
				}
				RefreshBoard ();
				break;
				
			case MotionEventActions.Up:
				if (floatingPiece.Active) {
					
					Piece piece = board [floatingPiece.Position];
					bool locked = ((blackLocked && BoardUtilities.IsBlack (piece)) || (whiteLocked && BoardUtilities.IsWhite (piece)));
					
					if (!locked) {
						Move move = null;

						if ((position > 0) && (board [position] == Piece.None)) {
							board [position] = board [floatingPiece.Position];
							board [floatingPiece.Position] = Piece.None;
							move = new Move (floatingPiece.Position, position);
						}
						
						floatingPiece.Position = FloatingPiece.INVALID_POSITION;
						
						if (move != null) {
							OnMoveInput (move);
						}
						RefreshBoard ();
					}
				}
				break;
			}

			return true;

		}
      
        public void SendData(Move move, int contin)
        {
            receiveFlag = false;
            SendRequest(this, new TArgs<string>
                             (string.Format("{0},{1}", move.Origin, move.Destination)));
            if (contin == 0) {
				LockPlayer (Player.Black, true);
				LockPlayer (Player.White, true);
			} else {
				LockPlayer (Player.Black, false);
				LockPlayer (Player.White, false);
			}
        }


        public void ReceiveData(string str)
        {
            receiveFlag = true;
            Move move = null;
            int origin , destin;
            string [] split = str.Split(new Char [] {',', ':'});

			origin = Convert.ToInt32(split[0]);
			destin = Convert.ToInt32(split[1]);

            move = new Move(origin, destin);
            if (move != null) {
                OnMoveInput (move);
            }
        }
      /// <summary>
      /// Invoked when a move has been made
      /// </summary>
      /// <param name="move"></param>
      protected virtual void OnMoveInput(Move move)
      {
         presenter.MakeMove(move, startPosition);
         audio.PlayWavResource(ctx, Resource.Raw.piece_drop1);
      }
      #endregion

		public void RefreshBoard()
		{
			PostInvalidate ();
			//Invalidate ();
		}


      #region Utility Methods


      /// <summary>
      /// Get the board position at the given coordinate
      /// </summary>
      /// <param name="coordinates">the coordinates</param>
      /// <returns>The board position for the given coordinate</returns>
      private int GetPosition(System.Drawing.PointF coordinates)
      {
         int squareSize = SquareSize;
         int col = BoardConstants.Cols - 1 - (int)(coordinates.X / squareSize);
         int row = BoardConstants.Rows - 1 - (int)(coordinates.Y / squareSize);
         return Checkers.Model.Location.ToPosition(row, col);
      }
      #endregion

      private void moveAnimator_MoveCompleted(Move move)
      {
         presenter.MakeMove(move);
    //        audio.PlayWavResource("piece_drop1.wav");
      }

      #region IBoardView Members

      /// <summary>
      /// Show the start message for the game
      /// </summary>
      public void ShowGameStart()
		{
			GameStarted = true;
			GameMessage = string.Empty;
			Console.WriteLine ("Game Started");
		}


      /// <summary>
      /// Set the board state to the state of the specified board
      /// </summary>
      /// <param name="board">The board to copy the state of</param>
      public void SetBoardState(IBoard board)
      {
         this.board.Copy(board);
			RefreshBoard ();
      }

      /// <summary>
      /// Indicate to the user the player with the current turn
      /// </summary>
      /// <param name="turn"></param>
      public void ShowPlayerChange(Player turn)
      {
         Console.WriteLine(string.Format(CultureInfo.InvariantCulture, "{0}'s turn", turn.ToString()));
      }

      /// <summary>
      /// Set the position that the player must begin the move from.  This usually
      /// indicates that the player must finish a move by completing a jump
      /// </summary>
      /// <param name="position">The position at which the current player must start the move</param>
      public void SetMoveStartPosition(int? position)
      {
         startPosition = position;
      }

      /// <summary>
      /// Allow or disallow the given player from moving
      /// </summary>
      /// <param name="player">The player</param>
      /// <param name="locked">If <c>true</c>, the player is prevented from moving.  If otherwise, the player is allowed to move</param>
      public void LockPlayer(Player player, bool locked)
      {
         Console.WriteLine(string.Format(CultureInfo.InvariantCulture, "Locking {0}: {1}", player.ToString(), locked.ToString(CultureInfo.InvariantCulture)));

         if (player == Player.Black)
         {
            blackLocked = locked;
         }
         else if (player == Player.White)
         {
            whiteLocked = locked;
         }
      }

      /// <summary>
      /// Prompt the given player to make move
      /// </summary>
      /// <param name="player">The player to prompt move for</param>
      public void PromptMove(Player player)
      {
         Console.WriteLine(string.Format(CultureInfo.InvariantCulture, "Waiting for {0}'s move input.", player.ToString()));
      }

      /// <summary>
      /// Prompt the player to make a move at the specified position.  This indicates that a move must be completed.
      /// </summary>
      /// <param name="player">The player to prompt</param>
      /// <param name="position">The position at which the player should start the move from</param>
      public void PromptMove(Player player, int position)
      {
         Console.WriteLine(string.Format(CultureInfo.InvariantCulture, "{0} must complete jump at {1}", player.ToString(), position));
      }

      /// <summary>
      /// Render the given move
      /// </summary>
      /// <param name="move">The move to render</param>
      /// <param name="after">The state the board should be in after the move is rendered</param>
      public void RenderMove(Move move, IBoard after)
      {

			moveAnimator.Start(move, SquareSize);
      }

      /// <summary>
      /// Indicate to the user that an invalid move was made
      /// </summary>
      /// <param name="move">The invalid move that was played</param>
      /// <param name="player">The player that made the move</param>
      /// <param name="message">An message indicating the problem with the move</param>
      public void ShowInvalidMove(Move move, Player player, string message)
      {
         Console.WriteLine(string.Format(CultureInfo.InvariantCulture, "Invalid Move: {0}", message));
         //MessageBox.Show(this, message, Application.ProductName);
      }

      /// <summary>
      /// Indicate that the game is over
      /// </summary>
      /// <param name="winner">The winner of the match</param>
      /// <param name="loser">The loser of the match</param>
      public void ShowGameOver(Player winner, Player loser)
      {
         GameStarted = false;
         GameMessage = string.Format(CultureInfo.CurrentCulture, "{0} WINS ! \r\n Click Board To Play", winner.ToString());
         audio.PlayWavResource(ctx, Resource.Raw.gamewin);
      }

      #endregion
   }
}
