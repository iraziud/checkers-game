using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using Android.Graphics;

namespace Checkers.Viewer.Providers.ViewModel
{
   /// <summary>
   /// The interface for a piece set
   /// </summary>
    public interface IPieceSet
    {
        /// <summary>
        /// Get the name of the piece set
        /// </summary>
        string Name { get;}

        /// <summary>
        /// Get black man image
        /// </summary>
		Bitmap BlackMan { get;}

        /// <summary>
        /// Get white man image
        /// </summary>
		Bitmap WhiteMan { get; }

        /// <summary>
        /// Get black king image
        /// </summary>
		Bitmap BlackKing { get;}

        /// <summary>
        /// Get white king image
        /// </summary>
		Bitmap WhiteKing { get; }

        /// <summary>
        /// Get dark square image
        /// </summary>
		Bitmap DarkSquare { get;}

        /// <summary>
        /// Get light square image
        /// </summary>
		Bitmap LightSquare { get; }
	}
}
