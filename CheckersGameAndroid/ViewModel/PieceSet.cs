using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using Android.App;
using Android.Graphics;
using Android.Content.Res;
using CheckersGameAndroid;

namespace Checkers.Viewer.Providers.ViewModel
{
    internal class PieceSet : IPieceSet
    {
        private readonly string name;
        private readonly Bitmap blackMan;
		private readonly Bitmap whiteMan;
		private readonly Bitmap blackKing;
		private readonly Bitmap whiteKing;
		private readonly Bitmap darkSquare;
		private readonly Bitmap lightSquare;

		public PieceSet(string name)
        {
            
			this.name = name;
			/*
			this.darkSquare = UIImage.FromFile ("darksquare.png");
			this.lightSquare = UIImage.FromFile ("lightsquare.png");
			*/
			Resources res = Application.Context.Resources;
			blackMan = Util.DecodeSampledBitmapFromResource (res, Resource.Drawable.black, 80, 80);
			whiteMan = Util.DecodeSampledBitmapFromResource (res, Resource.Drawable.white, 80, 80);
			blackKing = Util.DecodeSampledBitmapFromResource (res, Resource.Drawable.blackking, 80, 80);
			whiteKing = Util.DecodeSampledBitmapFromResource (res, Resource.Drawable.whiteking, 80, 80);
			darkSquare = null;
			lightSquare = null;
        }

        /// <summary>
        /// Get the name of the piece set
        /// </summary>
        public string Name { get { return name; } }

        /// <summary>
        /// Get black man image
        /// </summary>
		public Bitmap BlackMan { get { return blackMan; } }

        /// <summary>
        /// Get white man image
        /// </summary>
		public Bitmap WhiteMan { get { return whiteMan; } }

        /// <summary>
        /// Get black king image
        /// </summary>
		public Bitmap BlackKing { get { return blackKing; } }

        /// <summary>
        /// Get white king image
        /// </summary>
		public Bitmap WhiteKing { get { return whiteKing; } }

        /// <summary>
        /// Get dark square image
        /// </summary>
		public Bitmap DarkSquare { get { return darkSquare; } }

        /// <summary>
        /// Get light square image
        /// </summary>
		public Bitmap LightSquare { get { return lightSquare; } }
    }
}
