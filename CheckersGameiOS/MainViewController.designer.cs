// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace CheckersGame
{
	[Register ("MainViewController")]
	partial class MainViewController
	{
		[Action ("showInfo:")]
		partial void showInfo (MonoTouch.Foundation.NSObject sender);

		[Action ("showMultiInterface:")]
		partial void showMultiInterface (MonoTouch.Foundation.NSObject sender);

		[Action ("showOptionInterface:")]
		partial void showOptionInterface (MonoTouch.Foundation.NSObject sender);

		[Action ("showSingleInterface:")]
		partial void showSingleInterface (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
		}
	}
}
