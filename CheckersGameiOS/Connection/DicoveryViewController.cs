using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.MultipeerConnectivity;
using CheckersGame;


namespace Checkers.Multipeer.Connection
{
    //Base class for browser and advertiser view controllers
    public class DiscoveryViewController : UIViewController
    {
        public MCPeerID PeerID { get; set; }

        public MCSession Session { get; set; }

        protected const string SERVICE_STRING = "xam-checkers";

        public DiscoveryViewController(string peerID) : base()
        {
            PeerID = new MCPeerID(peerID);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            Session = new MCSession(PeerID);
            Session.Delegate = new MultiSessionDelegate(this);
        }

        public void Status(string str)
        {
            StatusChanged(this, new TArgs<string>(str));
        }
        public void Back () 
        {
            goback(this, null);
        }
        public event EventHandler goback;

        public event EventHandler<TArgs<string>> StatusChanged;
    }

    public class DiscoveryView : UIView
    {
        public event EventHandler Done = delegate{};
        readonly UIImageView imageview;

        public DiscoveryView(string roleName, DiscoveryViewController controller)
        {
            BackgroundColor = UIColor.White;
            UIImage backgroud = UIImage.FromFile("emptybackground.png");
            imageview = new UIImageView(new RectangleF(0,0,320, 568));
            imageview.Image = backgroud;
            AddSubview(imageview);

            var roleLabel = new UILabel(new RectangleF(50, 64, 200, 44)) {
                Text = roleName
            };
            AddSubview(roleLabel);

            var statusLabel = new UILabel(new RectangleF(50, 114, 200, 44));
            AddSubview(statusLabel);

            var backButton = new UIButton(UIButtonType.Custom){
                Frame =  new RectangleF(100, 174, 120, 44),
                Enabled = true
            };
            //backButton.SetTitle (" back ", UIControlState.Normal);
            backButton.SetImage(UIImage.FromFile("back.png"), UIControlState.Normal);
            AddSubview(backButton);

            backButton.TouchUpInside += (sender, e) => {
                if (Done != null)
                    Done (this, EventArgs.Empty);
                controller.Session.Dispose();
            };

            controller.StatusChanged += (s, e) => statusLabel.Text = e.Value;
            controller.goback += (sender, e) =>
            {
                if (Done != null)
                    Done(this, EventArgs.Empty);
                //controller.Session.Dispose();
            };
        }
        public override void Draw(RectangleF rect)
        {
            base.Draw(rect);
            InvokeOnMainThread(() =>
                               {
                imageview.Frame = rect;
            });
        }
    }

}

