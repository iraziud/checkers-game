using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.MultipeerConnectivity;

namespace Checkers.Multipeer.Connection
{
    public partial class RoleSelectController : UIViewController
    {
        string peerID;

        public RoleSelectController() : base ()
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            var rsv = new RoleSelectView();
            View = rsv;

            rsv.PeerNameEdited += (s, e) => {
                peerID = e.Value;
                rsv.EnableRoles(peerID.Length > 0);
            };

            rsv.RoleSelected += (s, e) => {
                var role = e.Value;
                switch(role)
                {
                    case Role.Advertiser: 
                        var adver = new AdvertiserController(peerID){
                            ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve,
                        };
                        PresentViewController(adver, true, null);
                        break;
                        case Role.Browser:
                        var browse =  new BrowserController(peerID){
                            ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve,
                        };
                        PresentViewController(browse, true, null);
                        break;
                }
            };
            rsv.Done += (s, e) => DismissViewController (true, null);
        }

    public class RoleSelectView : UIView
    {
        UIButton advertiserButton;
        UIButton browserButton;
        UIButton gomainButton;
        UIImageView imageview;

            private int width = 200;
            private int height = 44;

        public RoleSelectView()
        {
                int x = 60, y = 60;
            UIImage backgroud = UIImage.FromFile("emptybackground.png");
            imageview = new UIImageView(new RectangleF(0,0,320, 568));
            imageview.Image = backgroud;
            AddSubview(imageview);

                var label = new UILabel(new RectangleF(x, y, width, height)) {
                    Text = "Please Enter Your Name"
            };
            AddSubview(label);
                y += 50;
                var peerField = new UITextField(new RectangleF(x, y, width, height - 10)) {
                Placeholder = "Your Name"
            };
                peerField.BorderStyle = UITextBorderStyle.RoundedRect;
            AddSubview(peerField);
                y += 80;
            advertiserButton = new UIButton(UIButtonType.Custom) {
                    Frame = new RectangleF(x, y, width, height),
				Enabled = false
            };
            advertiserButton.SetImage(UIImage.FromFile("createServer.png"), UIControlState.Normal);
            AddSubview(advertiserButton);
                y += 80;
            browserButton = new UIButton(UIButtonType.Custom) {
                    Frame = new RectangleF(x, y, width, height),
                Enabled = false
            };
            browserButton.SetImage(UIImage.FromFile("connectClient.png"), UIControlState.Normal);
            AddSubview(browserButton);
                y += 80;
            gomainButton = new UIButton(UIButtonType.Custom){
                    Frame =  new RectangleF(x + 40 , y, width- 80, height),
                Enabled = true
            };
				gomainButton.SetImage(UIImage.FromFile("back.png"), UIControlState.Normal);
            gomainButton.SetTitle ("", UIControlState.Normal);
            AddSubview(gomainButton);

            peerField.EditingChanged += (sender, e) => PeerNameEdited(sender, new TArgs<String>(peerField.Text));
            advertiserButton.TouchUpInside += (sender, e) => RoleSelected(sender, new TArgs<Role>(Role.Advertiser));
            browserButton.TouchUpInside += (sender, e) => RoleSelected(sender, new TArgs<Role>(Role.Browser));
            gomainButton.TouchUpInside += (sender, e) => {
                 if (Done != null)
                        Done (this, EventArgs.Empty);
            };
        }

        public override void Draw(RectangleF rect)
        {
            base.Draw(rect);
            InvokeOnMainThread(() =>
            {
                imageview.Frame = rect;
            });
        }
        public void EnableRoles(bool shouldEnable)
        {
            advertiserButton.Enabled = shouldEnable;
            browserButton.Enabled = shouldEnable;
        }

        public event EventHandler<TArgs<String>> PeerNameEdited = delegate {};
        public event EventHandler<TArgs<Role>> RoleSelected = delegate {};
        public event EventHandler Done = delegate{};

    }

    }
}