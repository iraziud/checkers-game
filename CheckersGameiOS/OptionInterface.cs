using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Checkers.View.Providers.Controls;
namespace CheckersGame
{
    public class OptionInfo
    {
        public int Level{ get; set;}
        public int FirstPlayer { get; set;}
        public bool SoundState { get; set;}
        public OptionInfo(int level, int first, bool sound)
        {
            Level = level; FirstPlayer = first; SoundState = sound;
        }
    }

	public partial class OptionInterface : UIViewController
	{
		public event EventHandler Done;
        public OptionInfo option;

		public OptionInterface (OptionInfo option) : base ("OptionInterface", null)
		{
            this.option = option;
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{

			level_state.Text = "Normal";
			navigationbar.Hidden = false;
			start_btn.Hidden = true;
            if (option.FirstPlayer == 2) firstswitch.On = false;
            else firstswitch.On = true;

			if (option.Level == 2)
				levelslider.Value = 1f;
			else if (option.Level == 3)
				levelslider.Value = 2f;
			else
				levelslider.Value = 3f;

			base.ViewDidLoad ();
			// Perform any additional setup after loading the view, typically from a nib.
		}

		partial void done (UIBarButtonItem sender)
		{
			Console.WriteLine("welcome to OptionInterface" );
			if (Done != null)
				Done (this, EventArgs.Empty);
		}

		partial void chageFirst (NSObject sender)
		{
			if (firstswitch.On)
				option.FirstPlayer = 1;
			else
				option.FirstPlayer = 2;
		}

		partial void chageLevel (NSObject sender)
		{
			if (levelslider.Value >= 0f && levelslider.Value < 1.5f)
			{
				option.Level = 2 ;
				level_state.Text = "Easy";
			}
			else if (levelslider.Value >=1.5f && levelslider.Value < 2.5f)
			{
				option.Level = 3;
				level_state.Text = "Normal";
			}
			else{
				option.Level =4;
				level_state.Text = "Hard";
			}
		}
		partial void startGame (NSObject sender)
		{
			Console.WriteLine("welcome to OptionInterface" );
			if (Done != null)
				Done (this, EventArgs.Empty);
		}

	}
}

