using System;
using System.Collections.Generic;
using System.Text;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.CoreGraphics;

namespace Checkers.Viewer.Providers.ViewModel
{
    internal class PieceSet : IPieceSet
    {
        private readonly string name;
        private readonly UIImage blackMan;
		private readonly UIImage whiteMan;
		private readonly UIImage blackKing;
		private readonly UIImage whiteKing;
		private readonly UIImage darkSquare;
		private readonly UIImage lightSquare;

		public PieceSet(string name)
        {
            
			this.name = name;

			this.blackMan = UIImage.FromFile ("black.png");
			this.whiteMan = UIImage.FromFile ("white.png");
			this.blackKing = UIImage.FromFile ("blackking.png");
			this.whiteKing = UIImage.FromFile ("whiteking.png");
			this.darkSquare = UIImage.FromFile ("darksquare.png");
			this.lightSquare = UIImage.FromFile ("lightsquare.png");
        }

        /// <summary>
        /// Get the name of the piece set
        /// </summary>
        public string Name { get { return name; } }

        /// <summary>
        /// Get black man image
        /// </summary>
		public UIImage BlackMan { get { return blackMan; } }

        /// <summary>
        /// Get white man image
        /// </summary>
		public UIImage WhiteMan { get { return whiteMan; } }

        /// <summary>
        /// Get black king image
        /// </summary>
		public UIImage BlackKing { get { return blackKing; } }

        /// <summary>
        /// Get white king image
        /// </summary>
		public UIImage WhiteKing { get { return whiteKing; } }

        /// <summary>
        /// Get dark square image
        /// </summary>
		public UIImage DarkSquare { get { return darkSquare; } }

        /// <summary>
        /// Get light square image
        /// </summary>
		public UIImage LightSquare { get { return lightSquare; } }
    }
}
