using System;
using System.Collections.Generic;
using System.Text;

namespace Checkers.Viewer.Providers.ViewModel
{
   /// <summary>
   /// Factory for creating piece sets.
   /// </summary>
   public static class PieceSetFactory
   {
      private static IDictionary<string, IPieceSet> pieceSets = new Dictionary<string, IPieceSet>();

      /// <summary>
      /// Get names of keys
      /// </summary>
      /// <returns></returns>
      public static ICollection<string> PieceSetNames()
      {
         return pieceSets.Keys;
      }

      /// <summary>
      /// Create wood grain piece set
      /// </summary>
      /// <returns>wood grain piece set</returns>
      public static IPieceSet CreateWoodGrainPieceSet()
      {
			string pieceSetName = "Wood Grain";

         if (!pieceSets.ContainsKey(pieceSetName))
         {
				pieceSets.Add(pieceSetName, new PieceSet(pieceSetName));
         }

         return pieceSets[pieceSetName];
      }
   }
}
