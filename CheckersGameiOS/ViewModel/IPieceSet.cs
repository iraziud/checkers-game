using System;
using System.Collections.Generic;
using System.Text;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.CoreGraphics;

namespace Checkers.Viewer.Providers.ViewModel
{
   /// <summary>
   /// The interface for a piece set
   /// </summary>
    public interface IPieceSet
    {
        /// <summary>
        /// Get the name of the piece set
        /// </summary>
        string Name { get;}

        /// <summary>
        /// Get black man image
        /// </summary>
		UIImage BlackMan { get;}

        /// <summary>
        /// Get white man image
        /// </summary>
		UIImage WhiteMan { get; }

        /// <summary>
        /// Get black king image
        /// </summary>
		UIImage BlackKing { get;}

        /// <summary>
        /// Get white king image
        /// </summary>
		UIImage WhiteKing { get; }

        /// <summary>
        /// Get dark square image
        /// </summary>
		UIImage DarkSquare { get;}

        /// <summary>
        /// Get light square image
        /// </summary>
		UIImage LightSquare { get; }
	}
}
