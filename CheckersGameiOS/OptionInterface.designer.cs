// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace CheckersGame
{
	[Register ("OptionInterface")]
	partial class OptionInterface
	{
		[Outlet]
		MonoTouch.UIKit.UISwitch firstswitch { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel level_state { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISlider levelslider { get; set; }

		[Outlet]
		MonoTouch.UIKit.UINavigationBar navigationbar { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton start_btn { get; set; }

		[Action ("chageFirst:")]
		partial void chageFirst (MonoTouch.Foundation.NSObject sender);

		[Action ("chageLevel:")]
		partial void chageLevel (MonoTouch.Foundation.NSObject sender);

		[Action ("done:")]
		partial void done (MonoTouch.UIKit.UIBarButtonItem sender);

		[Action ("startGame:")]
		partial void startGame (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (firstswitch != null) {
				firstswitch.Dispose ();
				firstswitch = null;
			}

			if (level_state != null) {
				level_state.Dispose ();
				level_state = null;
			}

			if (levelslider != null) {
				levelslider.Dispose ();
				levelslider = null;
			}

			if (start_btn != null) {
				start_btn.Dispose ();
				start_btn = null;
			}

			if (navigationbar != null) {
				navigationbar.Dispose ();
				navigationbar = null;
			}
		}
	}
}
