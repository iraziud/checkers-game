using System;
using System.Collections.Generic;
using System.ComponentModel;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.CoreGraphics;
using MonoTouch.Foundation;

using System.Text;
using System.Globalization;
using Checkers.Model;
using Checkers.Viewer.Interfaces;
using Checkers.Presenter;
using Checkers.Viewer.Providers.ViewModel;
using Checkers.Multipeer.Connection;


namespace Checkers.View.Providers.Controls
{
   /// <summary>
   /// 2D board panel
   /// </summary>
	public partial class BoardPanel2D : UIView, IBoardView
   {
      private delegate void RefreshDelegate();
      private delegate void RenderMoveDelegate(Move move,IBoard after);

      public int Mode { get; set; }
      public int FirstPlayer { get; set; }
      public int Level{ get; set; }
        public bool receiveFlag{ get; set;}

      public event EventHandler<TArgs<string>> SendRequest = delegate {};

      #region Fields
      private readonly BoardPresenter presenter;
      private readonly IBoard board;
      private readonly MoveAnimator moveAnimator;
      private readonly FloatingPiece floatingPiece = new FloatingPiece();
      private readonly BoardPainter boardPainter;
      private int? startPosition;
      private bool blackLocked;
      private bool whiteLocked;
      private bool gameStarted;
      private string gameMessage;
      private int boardLength = 312;
        private AudioPlayer audio = new AudioPlayer();
      #endregion Fields


      /// <summary>
      /// Construct a board panel with the specified board
      /// </summary>
      public BoardPanel2D( int mode)
      {
         board = new Checkerboard();
         boardPainter = new BoardPainter(PieceSetFactory.CreateWoodGrainPieceSet());
         gameMessage = "Click Your Piece To Play";
         moveAnimator = new MoveAnimator(this, TimeSpan.FromMilliseconds(20), floatingPiece);
         moveAnimator.MoveCompleted += new MoveAnimator.MoveCompletedDelegate(moveAnimator_MoveCompleted);
         presenter = new BoardPresenter(this);
		 Mode = mode;
      }
        public BoardPanel2D(IMessager msgr, int mode)
        {
            board = new Checkerboard();
            boardPainter = new BoardPainter(PieceSetFactory.CreateWoodGrainPieceSet());
            gameMessage = "Click Your Piece To Play";
            moveAnimator = new MoveAnimator(this, TimeSpan.FromMilliseconds(20), floatingPiece);
            moveAnimator.MoveCompleted += new MoveAnimator.MoveCompletedDelegate(moveAnimator_MoveCompleted);
            presenter = new BoardPresenter(this);
            Mode = mode;
            msgr.MessageReceived += (s, e) => ReceiveData(e.Value);
        }
      /// <summary>
      /// Get size of board square
      /// </summary>
      private int SquareSize
      {
         get
         {
            return this.boardLength / BoardConstants.Rows;
         }
      }
	
      private string GameMessage
      {
         set
         {
            gameMessage = value;
			RefreshBoard ();
         }

         get { return gameMessage; }
      }


      #region Paint Board
      /// <summary>
      /// Raises the Draw event
      /// </summary>
	public override void Draw (RectangleF rect)
      {
		base.Draw (rect);
		using (CGContext context = UIGraphics.GetCurrentContext ()) {
				boardPainter.Paint (context, board, SquareSize, floatingPiece);
				context.ScaleCTM (1f, -1f);
				context.TranslateCTM (0, -Bounds.Height);
				string message = GameMessage;
				if (!string.IsNullOrEmpty (message)) {
                    new UIAlertView("Checkers Game", message, null, "OK", null).Show();
                    GameMessage = string.Empty;
                    Start();
				}
			}
      }
    #endregion
     public void Start()
        {
            if (!gameStarted)
            {
                Player humanPlayer = BoardUtilities.GetPlayer( Piece.BlackMan);
                Player computerPlayer = BoardUtilities.GetOpponent(humanPlayer);
                presenter.SetLevel (humanPlayer, Level);
                presenter.SetLevel (computerPlayer, Level);

                if (Mode == 1)
                { //single mode
                    presenter.SetComputer(humanPlayer, false);
                    presenter.SetComputer(computerPlayer, true);
                }
                else
                {
                    if (Mode == 2)
                    { //multi mode
                        presenter.SetComputer(humanPlayer, false);
                        presenter.SetComputer(computerPlayer, false);

                    }
                    else {
                        return;
                    }
                }

                presenter.FirstPlayer = FirstPlayer;
                presenter.StartGame();
                return;
            }
        }
	#region Touches Event Handlers
      /// <summary>
      /// Raises TouchesBegan event
      /// </summary>
      /// <param name="evt">The Tourches event args</param>
	  public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
	//		base.TouchesBegan (touches, evt);
			Console.WriteLine ("Touches Began");
			// get the touch
			UITouch touch = touches.AnyObject as UITouch;
			if (touch != null) { 
			
				PointF pt = touch.LocationInView (this);
			
			int position = GetPosition(pt);
            if (position > 0)
            {
               Piece piece = board[position];
               bool locked = ((blackLocked && BoardUtilities.IsBlack(piece)) || (whiteLocked && BoardUtilities.IsWhite(piece)));
               if ((!locked) && (piece != Piece.None))
               {
                        receiveFlag = false;
                  int offset = SquareSize / 2;
                  floatingPiece.X = (int)pt.X - offset;
                  floatingPiece.Y = (int)pt.Y - offset;
                  floatingPiece.Position = position;
				  RefreshBoard ();
               }
			Console.WriteLine(string.Format(CultureInfo.InvariantCulture, "clicked: {0}, piece={1}", position, piece));
            }
         }
      }

      /// <summary>
		/// Raises the TouchesMoved event
      /// </summary>
      /// <param name="e"></param>
   	 public override void TouchesMoved (NSSet touches, UIEvent evt)
		{
			base.TouchesMoved (touches, evt);
			// get the touch
			UITouch touch = touches.AnyObject as UITouch;
			if (touch != null) {
				PointF pt = touch.LocationInView (this);
				if ((!moveAnimator.Running) && (floatingPiece.Active)) {
					Piece piece = board [floatingPiece.Position];
					bool locked = ((blackLocked && BoardUtilities.IsBlack (piece)) || (whiteLocked && BoardUtilities.IsWhite (piece)));

					if (!locked) {
						int offset = SquareSize / 2;
						floatingPiece.X = (int)pt.X - offset;
						floatingPiece.Y = (int)pt.Y - offset;
					}
				}
				RefreshBoard ();
			}
      }
	
		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			Console.WriteLine ("Touches Ended");
			// get a reference to any of the touches
			UITouch touch = touches.AnyObject as UITouch;

			// if there is a touch
			if (touch != null) {

				// the point of touch
				PointF pt = touch.LocationInView (this);

				if (floatingPiece.Active) {

					Piece piece = board [floatingPiece.Position];
					bool locked = ((blackLocked && BoardUtilities.IsBlack (piece)) || (whiteLocked && BoardUtilities.IsWhite (piece)));

					if (!locked) {
						Move move = null;
						int position = GetPosition (pt);
						if ((position > 0) && (board [position] == Piece.None)) {
							board [position] = board [floatingPiece.Position];
							board [floatingPiece.Position] = Piece.None;
							move = new Move (floatingPiece.Position, position);
						}

						floatingPiece.Position = FloatingPiece.INVALID_POSITION;

						if (move != null) {
							OnMoveInput (move);
						}
						RefreshBoard ();
					}
				}
			}
      }

        public void SendData(Move move, int contin)
        {
            receiveFlag = false;
            SendRequest(this, new TArgs<string>
                             (string.Format("{0},{1}", move.Origin, move.Destination)));
            if (contin == 0)
            {
                LockPlayer(Player.Black, true);
                LockPlayer(Player.White, true);
            }
            else
            {
                LockPlayer(Player.Black, false);
                LockPlayer(Player.White, false);
            }
        }


        private void ReceiveData(string str)
        {
            receiveFlag = true;
            Move move = null;
            int origin , destin;
            string [] split = str.Split(new Char [] {',', ':'});

            origin = Convert.ToInt32(split[2]);
            destin = Convert.ToInt32(split[3]);

           // move = new Move(33 - origin, 33 - destin);
            move = new Move(origin, destin);
            if (move != null) {
                OnMoveInput (move);
            }
        }
      /// <summary>
      /// Invoked when a move has been made
      /// </summary>
      /// <param name="move"></param>
      protected virtual void OnMoveInput(Move move)
      {
         presenter.MakeMove(move, startPosition);
         audio.PlayWavResource("piece_drop1.wav");
      }
      #endregion

		public void RefreshBoard()
		{
			InvokeOnMainThread (delegate {
				SetNeedsDisplay ();
			});
		}


      #region Utility Methods


      /// <summary>
      /// Get the board position at the given coordinate
      /// </summary>
      /// <param name="coordinates">the coordinates</param>
      /// <returns>The board position for the given coordinate</returns>
      private int GetPosition(PointF coordinates)
      {
         int squareSize = SquareSize;
         int col = BoardConstants.Cols - 1 - (int)(coordinates.X / squareSize);
         int row = BoardConstants.Rows - 1 - (int)(coordinates.Y / squareSize);
         return Checkers.Model.Location.ToPosition(row, col);
      }
      #endregion

      private void moveAnimator_MoveCompleted(Move move)
      {
         presenter.MakeMove(move);
    //        audio.PlayWavResource("piece_drop1.wav");
      }

      #region IBoardView Members

      /// <summary>
      /// Show the start message for the game
      /// </summary>
      public void ShowGameStart()
		{
			gameStarted = true;
			GameMessage = string.Empty;
			Console.WriteLine ("Game Started");
		}


      /// <summary>
      /// Set the board state to the state of the specified board
      /// </summary>
      /// <param name="board">The board to copy the state of</param>
      public void SetBoardState(IBoard board)
      {
         this.board.Copy(board);
			RefreshBoard ();
      }

      /// <summary>
      /// Indicate to the user the player with the current turn
      /// </summary>
      /// <param name="turn"></param>
      public void ShowPlayerChange(Player turn)
      {
         Console.WriteLine(string.Format(CultureInfo.InvariantCulture, "{0}'s turn", turn.ToString()));
      }

      /// <summary>
      /// Set the position that the player must begin the move from.  This usually
      /// indicates that the player must finish a move by completing a jump
      /// </summary>
      /// <param name="position">The position at which the current player must start the move</param>
      public void SetMoveStartPosition(int? position)
      {
         startPosition = position;
      }

      /// <summary>
      /// Allow or disallow the given player from moving
      /// </summary>
      /// <param name="player">The player</param>
      /// <param name="locked">If <c>true</c>, the player is prevented from moving.  If otherwise, the player is allowed to move</param>
      public void LockPlayer(Player player, bool locked)
      {
         Console.WriteLine(string.Format(CultureInfo.InvariantCulture, "Locking {0}: {1}", player.ToString(), locked.ToString(CultureInfo.InvariantCulture)));

         if (player == Player.Black)
         {
            blackLocked = locked;
         }
         else if (player == Player.White)
         {
            whiteLocked = locked;
         }
      }

      /// <summary>
      /// Prompt the given player to make move
      /// </summary>
      /// <param name="player">The player to prompt move for</param>
      public void PromptMove(Player player)
      {
         Console.WriteLine(string.Format(CultureInfo.InvariantCulture, "Waiting for {0}'s move input.", player.ToString()));
      }

      /// <summary>
      /// Prompt the player to make a move at the specified position.  This indicates that a move must be completed.
      /// </summary>
      /// <param name="player">The player to prompt</param>
      /// <param name="position">The position at which the player should start the move from</param>
      public void PromptMove(Player player, int position)
      {
         Console.WriteLine(string.Format(CultureInfo.InvariantCulture, "{0} must complete jump at {1}", player.ToString(), position));
      }

      /// <summary>
      /// Render the given move
      /// </summary>
      /// <param name="move">The move to render</param>
      /// <param name="after">The state the board should be in after the move is rendered</param>
      public void RenderMove(Move move, IBoard after)
      {

			InvokeOnMainThread (delegate {
				moveAnimator.Start(move, SquareSize);
			});
      }

      /// <summary>
      /// Indicate to the user that an invalid move was made
      /// </summary>
      /// <param name="move">The invalid move that was played</param>
      /// <param name="player">The player that made the move</param>
      /// <param name="message">An message indicating the problem with the move</param>
      public void ShowInvalidMove(Move move, Player player, string message)
      {
         Console.WriteLine(string.Format(CultureInfo.InvariantCulture, "Invalid Move: {0}", message));
         //MessageBox.Show(this, message, Application.ProductName);
      }

      /// <summary>
      /// Indicate that the game is over
      /// </summary>
      /// <param name="winner">The winner of the match</param>
      /// <param name="loser">The loser of the match</param>
      public void ShowGameOver(Player winner, Player loser)
      {
         gameStarted = false;
         GameMessage = string.Format(CultureInfo.CurrentCulture, "{0} WINS ! \r\n Click Board To Play", winner.ToString());
         audio.PlayWavResource("gamewin.wav");
      }

      #endregion
   }
}
