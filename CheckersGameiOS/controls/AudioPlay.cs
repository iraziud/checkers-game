using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.AVFoundation;
using System.Diagnostics;
using System.IO;
using MonoTouch.CoreFoundation;

namespace Checkers.View.Providers.Controls
{
   /// <summary>
   /// Win 32 Audio player implementation
   /// </summary>
   public class AudioPlayer
   {
      
        AVPlayer player;

        public AudioPlayer()
        {

        }

      public void PlayWavResource(string wav)
      {
        NSError error = null;
        AVAudioSession.SharedInstance().SetCategory(AVAudioSession.CategoryPlayback, out error);
        if(error != null)
        {
            throw new Exception(error.DebugDescription);
        }
        if (!string.IsNullOrEmpty(wav))
         {
                this.player = new AVPlayer(NSUrl.FromFilename(wav));
                this.player.Play();
        }
      }

  }
}
