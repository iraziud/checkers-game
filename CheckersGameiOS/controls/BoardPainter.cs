using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.CoreGraphics;
using MonoTouch.Foundation;

using Checkers.Model;
using Checkers.Viewer.Providers.ViewModel;

namespace Checkers.View.Providers.Controls
{
   internal class BoardPainter : IDisposable
   {
      private UIImage boardImage;
      private int imageInset = 1;
      private IPieceSet pieceSet;

      
      public BoardPainter(IPieceSet pieceSet)
      {
         if (pieceSet == null)
         {
            throw new ArgumentNullException("pieceSet");
         }

         this.pieceSet = pieceSet;
      }

      
      public virtual void Dispose()
      {
         this.ClearCache();
         System.GC.SuppressFinalize(this);
      }
      

      public void ClearCache()
      {
         if (this.boardImage != null)
         {
            this.boardImage.Dispose();
         }
         this.boardImage = null;
      }

	  public void Paint(CGContext context, IBoard board, int squareSize, FloatingPiece floatingPiece)
      {
         UIImage boardImageCopy = this.boardImage;
         if (boardImageCopy == null)
         {
				boardImageCopy = this.boardImage = UIImage.FromFile ("mainboard.png");
         }
			PointF firstPos = new PointF (0, 0);
			RectangleF imageRect = new RectangleF (firstPos.X, firstPos.Y, boardImageCopy.CGImage.Width, boardImageCopy.CGImage.Height);
			context.DrawImage(imageRect, boardImageCopy.CGImage);
			Paint_piece(context, board, squareSize, floatingPiece);
      }

      private static int GetBoardWidth(int squareSize)
      {
         return BoardConstants.Cols * squareSize;
      }

      private static int GetBoardHeight(int squareSize)
      {
         return BoardConstants.Rows * squareSize;
      }

      private void Paint_piece(CGContext context, IBoard board, int squareSize, FloatingPiece floatingPiece)
      {
         int x = 0;
         int y = 0;
         int imageSize = squareSize - imageInset * 2;
         int position = BoardConstants.LightSquareCount;
		 RectangleF imageRect;
		
         for (int row = 0; row < BoardConstants.Rows; row++)
         {
            x = 0;

            for (int col = 0; col < BoardConstants.Rows; col++)
            {
               bool isDarkSquare = (row % 2 != col % 2);
               //
               // Draw square
               //
               if (isDarkSquare)
               {
                  //
                  // Draw piece on square
                  //
                     Piece piece = board[position];
                     if ((piece != Piece.None) && (floatingPiece.Position != position))
                     {
						imageRect = new RectangleF (x + imageInset, y + imageInset, imageSize, imageSize);
                        context.DrawImage(imageRect, GetPieceImage(piece).CGImage);
                     }

                  position--;
               }
               x += squareSize;
            }

            y += squareSize;
         }
         //
         // draw floating piece
         //
            if ((floatingPiece.Active) && (BoardUtilities.IsPiece(board[floatingPiece.Position])))
            {
               int floatingPieceSize = (int)(imageSize * 1.2);
               const int shadowOffset = 6;
               UIImage floatingPieceImage = GetPieceImage(board[floatingPiece.Position]);
				imageRect = new RectangleF (floatingPiece.X + shadowOffset, floatingPiece.Y + shadowOffset, floatingPieceSize, floatingPieceSize);
               context.FillEllipseInRect(imageRect);

				imageRect = new RectangleF (floatingPiece.X + imageInset, floatingPiece.Y + imageInset, floatingPieceSize, floatingPieceSize);
				context.DrawImage(imageRect, floatingPieceImage.CGImage);
            }

      }



      /// <summary>
      /// Get the image for the specified piece
      /// </summary>
      /// <param name="piece">the piece to get image for</param>
      /// <returns>The image for the piece</returns>
      private UIImage GetPieceImage(Piece piece)
      {
         switch (piece)
         {
            case Piece.BlackMan:
               return pieceSet.BlackMan;
            case Piece.WhiteMan:
               return pieceSet.WhiteMan;
            case Piece.BlackKing:
               return pieceSet.BlackKing;
            case Piece.WhiteKing:
               return pieceSet.WhiteKing;
            default:
               return null;
         }
      }

      /// <summary>
      /// Get the image for a square
      /// </summary>
      /// <param name="darkSquare">Is this a dark square</param>
      /// <returns>Dark square image iff <c>true</c> and light square if otherwise</returns>
      private UIImage GetSquareImage(bool darkSquare)
      {
         return (darkSquare) ? pieceSet.DarkSquare : pieceSet.LightSquare;
      }
   }
}
