using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Checkers.View.Providers.Controls;

namespace CheckersGame
{
	public partial class SimpleGameInterface : UIViewController
	{
		BoardPanel2D subview;

        public int Level{ get; set;}
        public int FirstPlayer { get; set;}

		public SimpleGameInterface (int firstplayer, int level) : base ("SimpleGameInterface", null)
		{
			FirstPlayer = firstplayer;
			Level = level;
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			int mode = 1; //single mode
			subview = new BoardPanel2D (mode);
			subview.Level  = Level;
			subview.FirstPlayer = FirstPlayer;

			subview.Frame = new RectangleF (4, 95, 312, 312);
			View.AddSubview (subview);

			// Perform any additional setup after loading the view, typically from a nib.
		}
		partial void goMenu (NSObject sender)
		{
			if (Done != null)
				Done (this, EventArgs.Empty);
		}

		public event EventHandler Done;

	}
}

