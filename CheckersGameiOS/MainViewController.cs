using System;
using System.Globalization;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Checkers.Multipeer.Connection;
using Checkers.Model;

namespace CheckersGame
{
	public partial class MainViewController : UIViewController
	{
        public OptionInfo globalOption = new OptionInfo( 3, 1, true);

		public MainViewController () : base ("MainViewController", null)
		{

		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		partial void showInfo (NSObject sender)
		{
            var controller = new FlipsideViewController () {
                ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve,
            };
			
			controller.Done += delegate {
				DismissViewController (true, null);
			};

			PresentViewController (controller, true, null);
		}
		partial void showSingleInterface (NSObject sender)
		{
			var controller = new  SimpleGameInterface(globalOption.FirstPlayer, globalOption.Level){
                ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve,
			};
			controller.Done +=delegate {
				DismissViewController (true, null);
			};

			PresentViewController (controller, true, null);
		}
		partial void showMultiInterface (NSObject sender)
		{
			
            var rsc = new RoleSelectController(){
				ModalTransitionStyle = UIModalTransitionStyle.CoverVertical,
            };
			PresentViewController (rsc, true, null);
		}

        partial void showOptionInterface (NSObject sender)
		{
            var controller = new OptionInterface (globalOption) {
				ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve,
			};

            controller.Done += delegate {
                globalOption = controller.option;
				DismissViewController (true, null);
			};
            PresentViewController (controller, true, null);
		}
	}
}

