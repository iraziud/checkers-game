using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.MultipeerConnectivity;

using Checkers.View.Providers.Controls;
using Checkers.Multipeer.Connection;

namespace CheckersGame
{
    public partial class MultiGameInterface : UIViewController, IMessager
	{
        protected MCSession Session { get; private set; }

        protected MCPeerID Me { get; private set; }

        protected MCPeerID Them { get; private set; }

        public int Level{ get; set;}
        public int FirstPlayer { get; set;}

        public MultiGameInterface(MCSession session, MCPeerID me, MCPeerID them, MultiSessionDelegate delObj) : base("MultiGameInterface", null)
        {
            this.Session = session;
            this.Me = me;
            this.Them = them;
            delObj.MultiController= this;
            FirstPlayer = 1;
        }

		public MultiGameInterface () : base ("MultiGameInterface", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			int mode = 2; // multi mode
			BoardPanel2D subview = new BoardPanel2D (this, mode);
            subview.Level  = 3;
            subview.FirstPlayer = FirstPlayer;
			subview.Frame = new RectangleF (4, 95, 312, 312);

			View.AddSubview (subview);
            subview.SendRequest += (s, e) => {
                var msg = e.Value;
                var peers = Session.ConnectedPeers;
                NSError error = null;
                Session.SendData(NSData.FromString(msg), peers, MCSessionSendDataMode.Reliable, out error);
                if(error != null)
                {
                    new UIAlertView("Error", error.ToString(), null, "OK", null).Show();
                }
            };

			// Perform any additional setup after loading the view, typically from a nib.
		}
		partial void goMenu (NSObject sender)
		{
			if (Done != null)
				Done (this, EventArgs.Empty);
           // Session.Disconnect();
            Console.WriteLine("go menu....session  released");
		}

		public event EventHandler Done;
 
        public void Message(string str)
        {
            MessageReceived(this, new TArgs<string>(str));
        }

        public event EventHandler<TArgs<string>> MessageReceived = delegate {};

	}

    public class MultiSessionDelegate : MCSessionDelegate
    {
        public DiscoveryViewController Parent{ get; protected set; }

        public MultiGameInterface MultiController
        {
            get; 
            set;
        }

        public MultiSessionDelegate(DiscoveryViewController parent)
        {
            Parent = parent;
        }

        public override void DidChangeState(MCSession session, MCPeerID peerID, MCSessionState state)
        {

            switch(state)
            {
                case MCSessionState.Connected:
                    Console.WriteLine("Connected to " + peerID.DisplayName);
                    InvokeOnMainThread(() =>{
                        MultiController = new MultiGameInterface(Parent.Session, Parent.PeerID, peerID, this){
                            ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve,
                        };
                        Parent.PresentViewController(MultiController, true, null);
                        MultiController.Done += delegate {
                            Parent.Back();
                        };
                    });
                    break;
                case MCSessionState.Connecting:
                    Console.WriteLine("Connecting to " + peerID.DisplayName);
                    break;
                    case MCSessionState.NotConnected:
                    Console.WriteLine("No longer connected to " + peerID.DisplayName);
                    break;
                    default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public override void DidReceiveData(MCSession session, MonoTouch.Foundation.NSData data, MCPeerID peerID)
        {

            if(MultiController != null)
            {
                InvokeOnMainThread(() => MultiController.Message(String.Format("{0} : {1}", peerID.DisplayName, data.ToString())));
            }
        }

        public override void DidStartReceivingResource(MCSession session, string resourceName, MCPeerID fromPeer, MonoTouch.Foundation.NSProgress progress)
        {
            InvokeOnMainThread(() => new UIAlertView("Msg", "DidStartReceivingResource()", null, "OK", null).Show());

        }

        public override void DidFinishReceivingResource(MCSession session, string resourceName, MCPeerID formPeer, MonoTouch.Foundation.NSUrl localUrl, out MonoTouch.Foundation.NSError error)
        {
            InvokeOnMainThread(() => new UIAlertView("Msg", "DidFinishReceivingResource()", null, "OK", null).Show());
            error = null;

        }

        public override void DidReceiveStream(MCSession session, MonoTouch.Foundation.NSInputStream stream, string streamName, MCPeerID peerID)
        {
            InvokeOnMainThread(() => new UIAlertView("Msg", "DidReceiveStream()", null, "OK", null).Show());

        }
    }



  
}

